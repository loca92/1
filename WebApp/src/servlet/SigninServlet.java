package servlet;





import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.RequestToken;
import twitter4j.conf.ConfigurationBuilder;

import java.io.IOException;

public class SigninServlet extends HttpServlet {
    private static final long serialVersionUID = -6205814293093350242L;
    Twitter twitter;
	ConfigurationBuilder cb = null;
	
	TwitterFactory tf = null;
	
	public SigninServlet() {
	    	super();
	        // TODO Auto-generated constructor stub
	    }
	
	public void init(ServletConfig config) {
			ServletContext context = config.getServletContext();
			String OAuthConsumerKey = context.getInitParameter("OAuthConsumerKey");
			String OAuthConsumerSecret = context.getInitParameter("OAuthConsumerSecret");
			cb = new ConfigurationBuilder();
			cb.setDebugEnabled(true)
			  .setOAuthConsumerKey(OAuthConsumerKey)
			  .setOAuthConsumerSecret(OAuthConsumerSecret)
			  .setOAuthRequestTokenURL("https://api.twitter.com/oauth/request_token")
			  .setOAuthAuthorizationURL("https://api.twitter.com/oauth/authorize")
			  .setOAuthAccessTokenURL("https://api.twitter.com/oauth/access_token");
			
	}
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	request.getSession(true).setAttribute("twitter", twitter);
    	 cb.setUseSSL(true);
		 tf = new TwitterFactory(cb.build());
		
		 twitter = tf.getInstance();
    		try {
        		
        		StringBuffer callbackURL = request.getRequestURL();
        		int index = callbackURL.lastIndexOf("/");
        		System.out.println(callbackURL);
        		callbackURL.replace(index, callbackURL.length(), "").append("/callback");
        		System.out.println(callbackURL);
        		RequestToken requestToken = twitter.getOAuthRequestToken(callbackURL.toString());
        	
        		request.getSession().setAttribute("requestToken", requestToken);
        		response.sendRedirect(requestToken.getAuthenticationURL());
           		
        	} catch (TwitterException e) {
        		System.out.println(e.getMessage());
        	}
    		response.sendRedirect("index.jsp");
 
        
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    
    }
    
}
