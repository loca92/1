package servlet;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.RequestToken;
import twitter4j.conf.ConfigurationBuilder;

/**
 * Servlet implementation class ServLogin
 */

public class ServLogin extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	Twitter twitter;

	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServLogin() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    public void init(ServletConfig config) {
    
    	
    	TwitterFactory tf;
    	/*ServletContext context = config.getServletContext();
    	String OAuthConsumerKey = context.getInitParameter("OAuthConsumerKey");
		String OAuthConsumerSecret = context.getInitParameter("OAuthConsumerSecret");
		*/
    	String OAuthConsumerKey = "2YtFe0UQcr4PsdV1Ys6aDu0jT";
    	String OAuthConsumerSecret = "mN2YPZBCG6YKyY56FwbaH5jwmChDXP8dpSAacDdzHLSTDjp9UH";
		
		ConfigurationBuilder cb = new ConfigurationBuilder();
		cb.setDebugEnabled(true)
		  .setOAuthConsumerKey(OAuthConsumerKey)
		  .setOAuthConsumerSecret(OAuthConsumerSecret)
		  .setOAuthRequestTokenURL("https://api.twitter.com/oauth/request_token")
		  .setOAuthAuthorizationURL("https://api.twitter.com/oauth/authorize")
		  .setOAuthAccessTokenURL("https://api.twitter.com/oauth/access_token");
		tf = new TwitterFactory(cb.build());
		twitter = tf.getInstance();
    }
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		try {
			request.getSession().setAttribute("twitter", twitter);
			StringBuffer callbackURL = request.getRequestURL();
    		int index = callbackURL.lastIndexOf("/");
    		callbackURL.replace(index, callbackURL.length(), "").append("/callback");
    		
    		RequestToken requestToken = twitter.getOAuthRequestToken(callbackURL.toString());
    		request.getSession().setAttribute("requestToken", requestToken);
    		response.sendRedirect(requestToken.getAuthenticationURL());
       		
    	} catch (TwitterException e) {
    		System.out.println(e.getMessage());
    	}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
