<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Somma con sessione</title>
</head>
<body>
	
	<% 
	//Inizializzazione sessione
	   Object val=null;
	   val= session.getAttribute("risultato");
	%>
	
	<%
	   if(val==null){
		   
		%>
	
	<!-- in action c'è l'URL della servlet (nome) -->
	<form name="numeriIniziali" action="ServSessione" method="POST">
		Primo numero: 
		<input type="text" name="num1"><br>
		Secondo numero: 
		<input type="text" name="num2">
		<input type="submit" value="Invia">
	</form>
	
	<%
	} else {
		%>
		Il risultato e':
		<%=val %>
		<br>
		<br>
		<form action="ServSessione" method="POST">
			<input type="submit" value="Nuova somma">
		</form>
		
<%} %>

</body>
</html>