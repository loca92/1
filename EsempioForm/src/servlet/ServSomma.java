package servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ServSomma
 */
@WebServlet("/ServSomma")
public class ServSomma extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServSomma() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		response.setContentType("text/plain");
		PrintWriter out = response.getWriter();
		out.println("Dal caricamento, e dopo eventuali azzeramenti");	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		int numero1=Integer.parseInt(request.getParameter("num1")); //trasformo in intero il parametro preso in ingresso come stringa
		int numero2=Integer.parseInt(request.getParameter("num2"));
		String ris= Integer.toString(numero1+numero2);
		request.setAttribute("risultato", ris);
		request.getRequestDispatcher("risultati.jsp").forward(request, response);
	}

}
