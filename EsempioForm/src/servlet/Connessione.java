package servlet;

import java.io.*;
import java.sql.*;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

@WebServlet("/Connessione")

public class Connessione extends HttpServlet {

	private static final long serialVersionUID = 9075587038246261767L;

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Connection connection = null;

		response.setContentType("text/plain");
		PrintWriter out = response.getWriter();

		try {
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/test", "root", "");
			// prendo i parametri dalla richiesta
			String username = request.getParameter("nickname");
			String password = request.getParameter("password");

			if (username == "" || password == ""){
				out.println("Errore parametri vuoti");
			} else {

				PreparedStatement statement = connection.prepareStatement("SELECT * FROM Utente WHERE nickname=? AND password=?");
				statement.setString(1, username);
				statement.setString(2, password);

				ResultSet result = statement.executeQuery();
				result.next();
				if(result != null) {
					int id = result.getInt(1);
					out.println("Login effettuato "+ id);

				}

				result.close();
				statement.close();


			}
			/*out.println("Phone numbers:");
			while (resultSet.next())
				out.println("- " + resultSet.getString("idUtente"));

			while (resultSet.next()) {
				if()
			}*/

		}
		catch (ClassNotFoundException e) {
			out.println("Couldn't load database driver: " + e.getMessage());
		}
		catch (SQLException e) {
			out.println("SQLexception caught: " + e.getMessage());
		}
		finally {
			if (connection != null) {
				try {
					connection.close();
				}
				catch (SQLException ignored) {
				}
			}
		}
	}
}
